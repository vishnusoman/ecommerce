import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import toast from 'react-hot-toast'
import {decode} from 'html-entities';

export const addToCart = createAsyncThunk(
    'cart/addToCart',
    async (id) =>{
      toast.remove()
        toast.loading('Adding to cart...')
        const responce = await axios({
          method: 'post',
          url: '/wp-json/cocart/v2/cart/add-item',
          data: {
            id: id.toString(),
            quantity: '1',
          }
        })
        return responce.data
    }
)

export const getCartData = createAsyncThunk(
    'cart/cartData',
    async (prev) =>{
        const responce = await axios({
          method: 'get',
          url: '/wp-json/cocart/v2/cart',
        })
        return responce.data
    }
)

export const removeCartItem = createAsyncThunk(
    'cart/removeCartItem',
    async (id) =>{
      toast.loading('Updating cart...')
      const responce = await axios({
        method: 'delete',
        url: '/wp-json/cocart/v2/cart/item/'+id,
      })
      return responce.data
    }
)

export const updateCartItem = createAsyncThunk(
    'cart/updateCartItem',
    async (data) =>{
      toast.remove()
      toast.loading('Updating cart...')
      const responce = await axios({
        method: 'POST',
        url: '/wp-json/cocart/v2/cart/item/'+data.id,
        data: {
            quantity: data.quantity.toString(),
          }
      })
      return responce.data
    }
)


export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    count: 0,
    total: 0,
    item:[],
    isLoading: false
  },
  reducers: {
    addItem(state, action) {
        
        state.count += 1
        state.item.push(action.payload)
    }
  },
  extraReducers: {
    [addToCart.pending]:(state) =>{
        state.isLoading = true
        
    },
    [addToCart.fulfilled]:(state, action) =>{
        state.isLoading = false
        state.count = action.payload.item_count
        state.total = action.payload.totals.total
        state.item = action.payload.items
        toast.remove()
        action.payload.notices.success.map((item,index) => { 
          return toast.success(decode(item),{
            duration: 4000,
            position: 'top-right',
          })
        })
    },
    [addToCart.rejected]:(state, action) =>{
        state.isLoading = false
    },



    [removeCartItem.pending]:(state) =>{
        state.isLoading = true
        
    },
    [removeCartItem.fulfilled]:(state, action) =>{
      console.log(action);
        state.isLoading = false
        state.count = action.payload.item_count
        state.total = action.payload.totals.total
        state.item = action.payload.items
        toast.remove()
        action.payload.notices.success.map((item,index) => { 
          return toast.success(decode(item),{
            duration: 4000,
            position: 'top-right',
          })
        })
    },
    [removeCartItem.rejected]:(state, action) =>{
        state.isLoading = false
    },



    [getCartData.pending]:(state, action) =>{
        state.isLoading = true
    },
    [getCartData.fulfilled]:(state, action) =>{
        state.isLoading = false
        state.count = action.payload.item_count
        state.total = action.payload.totals.total
        state.item = action.payload.items
    },

    

    [updateCartItem.pending]:(state, action) =>{
        state.isLoading = true
    },
    [updateCartItem.fulfilled]:(state, action) =>{
        toast.remove()
        state.isLoading = false
        state.count = action.payload.item_count
        state.total = action.payload.totals.total
        state.item = action.payload.items
    },

  }
})

export const { addItem } = cartSlice.actions

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
// export const incrementAsync = (amount) => (dispatch) => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount))
//   }, 1000)
// }

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCount = (state) => state.counter.value

export default cartSlice.reducer
