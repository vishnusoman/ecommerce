// import {createStore} from 'redux'
// const initialState = {
//     count: 0,
//     items:[]
// }

// function CartReducer(prvState = initialState, action) {
//     console.log(prvState,action)
//   return prvState
// }

// const store = createStore(CartReducer,
// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

// export default store

import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../Store/Slice/CartSlice';
import logger from 'redux-logger'

export default configureStore({
  reducer: {
    cart: counterReducer,
  },
  middleware:(getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});
