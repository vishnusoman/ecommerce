import React,{useState, useEffect} from 'react'
import Breadcrumb from '../Components/Product/Breadcrumb'
import axios from "axios";
import { useLoginContext } from '../Context/LoginContext';
import AlertError from '../Components/Site/AlertError';
import { useNavigate, useLocation } from "react-router-dom";


function Login({keySet}) {
    const path = [
        {path:'/',name:'Home'},
        {name:'Login'},
    ]
    const [loginData,setLoginData] = useState({email:'', password:''})
    const [loginError,setLoginError] = useState({})
    const [loginLoader,setLoginLoader] = useState(true)
    const [loginBtnStatus,setLoginBtnStatus] = useState(true)
    const handleLoginChange = (event) => { 
        const name = event.target.name;
        const value = event.target.value;
        setLoginData(values => ({...values, [name]: value}))
    }
    const getUserData = useLoginContext();
    let navigate  = useNavigate();
    let location  = useLocation();

    
    const loginHandle = () => {
            setLoginLoader(true)
            setLoginBtnStatus(false)
            axios.post(
                '?rest_route=/simple-jwt-login/v1/auth&'+new URLSearchParams(loginData).toString(),
            ).then((response) => {
                let resp =  response.data;
                setLoginLoader(false)
                const jwtToken = resp.data.jwt
                localStorage.setItem('jwt', jwtToken)
                keySet(jwtToken)
                axios.get('?rest_route=/simple-jwt-login/v1/auth/validate&jwt='+jwtToken
                ).then((resp) => {
                    //console.log(resp);
                    getUserData.setUserData((prv) => (resp.data))
                    (location !== "/login") ? navigate(location) : navigate("/home")
                    
                    setLoginError({
                        message:resp.data.message,
                        type:'success'
                })
                }).catch((error) => {
                    //console.log("Problem submitting New Post", error);
                });
                
                
            }).catch((error) => {
                //console.log(error.response.data.data.message)
                setLoginError({
                    message:error.response.data.data.message,
                    type:'danger'
                })
                //console.log("Problem submitting New Post", error);
            });
    }
    

    useEffect(() => {
        (loginData.email.trim().length && loginData.password.trim().length) ?
        setLoginBtnStatus(true) : setLoginBtnStatus(false)
        
    }, [loginData]);

    
  return (
    <>
    <Breadcrumb path={path} />
    <div className="container">
        <div className="row mt-4 mb-4">
            <div className="col-lg-6">
                <div className="checkout__input">
                    <p>Name<span>*</span></p>
                    <input type="text" />
                </div>
                <div className="checkout__input">
                    <p>Email<span>*</span></p>
                    <input type="text" />
                </div>
                <div className="checkout__input">
                    <p>Password<span>*</span></p>
                    <input type="password" />
                </div>
                <div className="checkout__input">
                    <p>Conirm Password<span>*</span></p>
                    <input type="password" />
                </div>
                <button type="submit" className="site-btn">Register</button>
            </div>
            <div className={`col-lg-6 ${!loginLoader && 'disabled'}`}>
                {loginError.message ? <AlertError errorData={loginError}/> : ""}
                <div className="checkout__input">
                    <p>Email</p>
                    <input type="text" onChange={handleLoginChange} name="email" value={loginData.email} />
                </div>
                <div className="checkout__input">
                    <p>Password</p>
                    <input type="password" onChange={handleLoginChange} name="password" value={loginData.password} />
                </div>
                <button type="submit" className={`site-btn ${!loginBtnStatus && 'disabled'}`} onClick={loginHandle}>Login</button>
            </div>
        </div>
    </div>
    </>
  )
}

export default Login
