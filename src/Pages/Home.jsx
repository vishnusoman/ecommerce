import {useEffect, useState} from 'react'
import axios from 'axios'
import Grid from '../Components/Product/Grid';
import BlogList from '../Components/Blogs/BlogList';
import {useDispatch} from 'react-redux'

function Home() {
     //const test = useSelector((state) => { return state })
     const dispatch = useDispatch()
     //console.log(test);

    const [products, setProducts] = useState([])
    const [productTab, setProductTab] = useState('best')
    const [productLoad, setProductLoad] = useState(true)
    
    useEffect(() => {
        setProductLoad(true)
        axios.get('/wp-json/wc/v3/products?orderby=date&order=asc&per_page=8'
        ).then((response) => {
            setProductLoad(false)
            setProducts(response.data)
        })

        

    }, []);

    useEffect(() => {
        //console.log(1111)
        //dispatch(addToCart())
    }, [dispatch]);
    

    const productTabChange = (type) => { 
        setProductTab(type)
        var tabUrl="";
        switch(type) {
            case "hot": tabUrl = "/wp-json/wc/v3/products?featured=1&per_page=8";
                break;
            case "new": tabUrl = "/wp-json/wc/v3/products?orderby=date&order=asc&per_page=8";
                break;
            default: tabUrl = "/wp-json/wc/v3/products?orderby=date&order=desc&per_page=8";
        }
        setProductLoad(true)
        axios.get(tabUrl
        ).then((response) => {
            setProductLoad(false)
            setProducts(response.data)
        });
    }

   
    
  return (
    <>
    
    <section className="hero">
        <div className="hero__slider owl-carousel">
            <div className="hero__items set-bg" style={{'backgroundImage': 'url(assets/img/hero/hero-1.jpg)'}}>
                <div className="container">
                    <div className="row">
                        <div className="col-xl-5 col-lg-7 col-md-8">
                            <div className="hero__text">
                                <h6>Summer Collection</h6>
                                <h2>Fall - Winter Collections 2030</h2>
                                <p>A specialist label creating luxury essentials. Ethically crafted with an unwavering
                                commitment to exceptional quality.</p>
                                <a href="/#" className="primary-btn">Shop now <span className="arrow_right"></span></a>
                                <div className="hero__social">
                                    <a href="/#"><i className="fa fa-facebook"></i></a>
                                    <a href="/#"><i className="fa fa-twitter"></i></a>
                                    <a href="/#"><i className="fa fa-pinterest"></i></a>
                                    <a href="/#"><i className="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="hero__items set-bg" style={{'background': 'url(assets/img/hero/hero-2.jpg)'}}>
                <div className="container">
                    <div className="row">
                        <div className="col-xl-5 col-lg-7 col-md-8">
                            <div className="hero__text">
                                <h6>Summer Collection</h6>
                                <h2>Fall - Winter Collections 2030</h2>
                                <p>A specialist label creating luxury essentials. Ethically crafted with an unwavering
                                commitment to exceptional quality.</p>
                                <a href="/#" className="primary-btn">Shop now <span className="arrow_right"></span></a>
                                <div className="hero__social">
                                    <a href="/#"><i className="fa fa-facebook"></i></a>
                                    <a href="/#"><i className="fa fa-twitter"></i></a>
                                    <a href="/#"><i className="fa fa-pinterest"></i></a>
                                    <a href="/#"><i className="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="banner spad">
        <div className="container">
            <div className="row">
                <div className="col-lg-7 offset-lg-4">
                    <div className="banner__item">
                        <div className="banner__item__pic">
                            <img src="assets/img/banner/banner-1.jpg" alt="" />
                        </div>
                        <div className="banner__item__text">
                            <h2>Clothing Collections 2030</h2>
                            <a href="/shop">Shop now</a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-5">
                    <div className="banner__item banner__item--middle">
                        <div className="banner__item__pic">
                            <img src="assets/img/banner/banner-2.jpg" alt="" />
                        </div>
                        <div className="banner__item__text">
                            <h2>Accessories</h2>
                            <a href="/shop">Shop now</a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-7">
                    <div className="banner__item banner__item--last">
                        <div className="banner__item__pic">
                            <img src="assets/img/banner/banner-3.jpg" alt="" />
                        </div>
                        <div className="banner__item__text">
                            <h2>Shoes Spring 2030</h2>
                            <a href="/shop">Shop now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="product spad">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <ul className="filter__controls">
                        <li className={productTab === 'best'?'active':''} onClick={() => { productTabChange('best') }}>Best Sellers</li>
                        <li className={productTab === 'new'?'active':''} onClick={() => { productTabChange('new') }}>New Arrivals</li>
                        <li className={productTab === 'hot'?'active':''} onClick={() => { productTabChange('hot') }}>Hot Sales</li>
                    </ul>
                </div>
            </div>
            <div className="row product__filter">
                {
                products.length > 0 ?
                products.map((data) => (
                    <div className="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 " key={data.id}>
                        <Grid
                        id = {data.id}
                        title = {data.name}
                        img = {data.images[0].src}
                        price = {'₹'+data.price}
                        loading = {productLoad}
                        average_rating = {data.average_rating}
                        />
                    </div>
                )) :
                [...Array(8)].map((elementInArray, index) => ( 
                    <div className="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 " key={index} >
                        <Grid loading = {productLoad} />
                    </div>
                ))
                
            }
            </div>
        </div>
    </section>

    <BlogList />

    </>
  )
}

export default Home