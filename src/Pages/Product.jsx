import Breadcrumb from '../Components/Product/Breadcrumb'
import { useParams } from "react-router-dom";
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Placeholder } from 'react-bootstrap';


function Product() {
    const path = [
        {path:'/',name:'Home'},
        {path:'/shop',name:'Products'},
        {name:'produxt'},
    ]
    const params = useParams();
    const [product, setProduct] = useState({
      isLoading: true,
      item: []
    })
    console.log(product);
    const SingleLoad = () => { 
        return(
          <>
          <div className="wrapper row">
            <div className="preview col-md-6">
              <Placeholder animation="glow">
                  <Placeholder xs={12}  style={{ height: '400px' }}/>
              </Placeholder>
            </div>
            <div className="details col-md-6">
                <Placeholder as="h3" className="product-title" animation="glow">
                    <Placeholder   xs={6}/>
                </Placeholder>
                <Placeholder animation="glow">
                    <Placeholder   xs={3}/>
                </Placeholder>
                <p className="product-description mt-3">
                <Placeholder animation="glow">
                    <Placeholder   xs={4}/>
                    <Placeholder  className="ml-2" xs={3}/>
                    <Placeholder  className="ml-2" xs={2}/>
                    <Placeholder  className="" xs={5}/>
                    <Placeholder  className="ml-2" xs={4}/>
                    <Placeholder  className="" xs={3}/>
                    <Placeholder  className="ml-2" xs={2}/>
                    <Placeholder  className="ml-2" xs={5}/>
                    <Placeholder  className="" xs={4}/>
                    <br/>
                    <Placeholder.Button  className="mt-5" bg="dark" xs={4}/>
                    <Placeholder.Button className="ml-2 mt-5" bg="dark" xs={1}/>


                </Placeholder>
                  
                </p>
                
            </div>
          </div>
          </>
        )
     }
    
    const SingleData = () => { 
        return(
          <>
          <div className="wrapper row">
            <div className="preview col-md-6">
                <img alt="" src="http://placekitten.com/400/252" className='w-100 img-fluid rounded-3 m-2' />
            </div>
            <div className="details col-md-6">
                <h3 className="product-title">{product.item.name}</h3>
                <div className="rating">
                  <div className="stars">
                      <span className="fa fa-star checked"></span>
                      <span className="fa fa-star checked"></span>
                      <span className="fa fa-star checked"></span>
                      <span className="fa fa-star"></span>
                      <span className="fa fa-star"></span>
                  </div>
                  <div className="rating">
                    <i className={`fa  mr-1 ${product.item.average_rating>=1 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${product.item.average_rating>=2 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${product.item.average_rating>=3 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${product.item.average_rating>=4 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${product.item.average_rating>=5 ? 'fa-star':'fa-star-o'}`}></i>
                </div>
                  <span className="review-no">41 reviews</span>
                </div>
                <p className="product-description">{product.item.short_description}</p>
                <h4 className="price">current price: <span>₹{product.item.price}</span></h4>
                <p className="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>
                <h5 className="sizes">sizes:
                  <span className="size" data-toggle="tooltip" title="small">s</span>
                  <span className="size" data-toggle="tooltip" title="medium">m</span>
                  <span className="size" data-toggle="tooltip" title="large">l</span>
                  <span className="size" data-toggle="tooltip" title="xtra large">xl</span>
                </h5>
                <h5 className="colors">colors:
                  <span className="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
                  <span className="color green"></span>
                  <span className="color blue"></span>
                </h5>
                <div className="action">
                  <button className="primary-btn" type="button">add to cart</button>
                  <button className="primary-btn" type="button"><span className="fa fa-heart"></span></button>
                </div>
            </div>
          </div>
          </>
        )
     }
    useEffect(() => {
      
        axios.get('/wp-json/wc/v3/products/'+params.id
        ).then((response) => {
          console.log(response)
          let getProduct = {...product}
          getProduct.isLoading = false
          getProduct.item = response.data
          setProduct( getProduct )
        })

        

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
  return (
    <>
        <Breadcrumb path={path} isLoading={product.isLoading}/>
        	<div className="container">
            <div className="card mt-4 mb-4">
              <div className="container-fliud">
                {
                  product.isLoading === true ? 
                  <SingleLoad /> :
                  <SingleData />
                }
              </div>
            </div>
          </div>
    </>
  )
}

export default Product