import {useEffect, useState} from 'react'
import axios from 'axios'
import Grid from '../Components/Product/Grid'
import ProductSidebar from '../Components/Product/ProductSidebar'
import Breadcrumb from '../Components/Product/Breadcrumb'
import ReactPaginate from 'react-paginate'

function Shop() {
    const [products, setProducts] = useState([])
    const [productLoad, setProductLoad] = useState(true)
    const [pageData, setpageData] = useState({
        total: 0,
        now: 0,
        resultCount:0
    })
    const [filter, SetFilter] = useState({
        orderby: 'date',
        order: 'asc',
        page: 1,
        category:'',
        tag:'',
        search:''
    });
    
    
    useEffect(() => {
        setProductLoad(true)
    }, []);

    const handlePagination = (event) => { 
        const page = event.selected +1;
        setProductLoad(true)
        axios.get('/wp-json/wc/v3/products?'+new URLSearchParams(filter).toString()+'&per_page=12'
        ).then((response) => {
            setProductLoad(false)
            setProducts(response.data)
            setpageData({
                total: response.headers['x-wp-totalpages'],
                now: page,
                resultCount:response.headers['x-wp-total']
            }) 
            let pageFilter = {...filter};
            pageFilter.page = page
            SetFilter(pageFilter)
        });
    }

    const sortHandle = (e) => { 
        let sortFilter = {...filter};
        
        switch(e.target.value) {
            case "popularity":
                sortFilter.orderby = 'popularity'
                sortFilter.order = 'asc'
            
                break;
            case "rating": 
                sortFilter.orderby = 'rating'
                sortFilter.order = 'asc'
            
                break;
            case "date": 
                sortFilter.orderby = 'date'
                sortFilter.order = 'asc'
            
                break;
            case "price":
                sortFilter.orderby = 'price'
                sortFilter.order = 'asc'
            
                break;
            case "price-desc": 
                sortFilter.orderby = 'price-desc'
                sortFilter.order = 'asc'
            
                break;
            default: 
                sortFilter.orderby = 'date'
                sortFilter.order = 'asc'
            
        }
        SetFilter(sortFilter)
        
    }

    useEffect(() => {
        setProductLoad(true)
        axios.get('/wp-json/wc/v3/products?per_page=12&'+new URLSearchParams(filter).toString()
        ).then((response) => {
            setProductLoad(false)
            setProducts(response.data)
            setpageData({
                total: response.headers['x-wp-totalpages'],
                now: 1,
                resultCount:response.headers['x-wp-total']
            }) 
            
        });
    }, [filter]);
    const path = [
        {path:'/',name:'Home'},
        {name:'Shop'},
    ]

  return (
    <>
    <Breadcrumb path={path} />
    <section className="shop spad">
        <div className="container">
            <div className="row">
                <ProductSidebar categoryfilter={SetFilter} filterData={filter} />                      
                <div className="col-lg-9">
                    <div className="shop__product__option">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="shop__product__option__left">
                                    {(pageData.total>0)? <p>Showing {pageData.now}–{pageData.total} of {pageData.resultCount} results</p>:''}
                                    
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="shop__product__option__right">
                                    <p>Sort by Price:</p>
                                    <select onChange={sortHandle} >
                                        <option value="menu_order" selected="selected">Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="rating">Sort by average rating</option>
                                        <option value="date">Sort by latest</option>
                                        <option value="price">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {products.map((data) => (
                            <div className="col-lg-4 col-md-6 col-sm-6" key={data.id}>
                                <Grid
                                id = {data.id}
                                title = {data.name}
                                img = {data.images[0].src}
                                price = {'₹'+data.price}
                                loading = {productLoad}
                                average_rating = {data.average_rating}
                                />
                            </div>
                        )) }
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="product__pagination">
                                { pageData.total>1?
                                    <ReactPaginate
                                        breakLabel="..."
                                        nextLabel=">"
                                        className="list-unstyled list-inline"
                                        pageClassName="list-inline-item"
                                        breakClassName="list-inline-item"
                                        previousClassName="list-inline-item"
                                        nextClassName="list-inline-item"
                                        activeLinkClassName="active"
                                        pageRangeDisplayed={12}
                                        pageCount= {pageData.total}
                                        previousLabel="<"
                                        renderOnZeroPageCount={null}
                                        onPageChange={handlePagination}
                                    />: ""
                                }
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    </>
  )
}

export default Shop