import React from 'react'
import { useSelector } from 'react-redux'
import Breadcrumb from '../Components/Product/Breadcrumb'
import CartItem from '../Components/Product/CartItem'


function Cart() {
    const path = [
        {path:'/',name:'Home'},
        {path:'/shop',name:'Shop'},
        {name:'Shopping Cart'},
    ]
    //const dispatch = useDispatch()
    const cartData = useSelector((prev) => { return prev })
  return (
    <>
        <Breadcrumb path={path} />
        <section className="shopping-cart spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8">
                        <div className="shopping__cart__table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {   cartData.cart.item.length > 0 ?
                                        cartData.cart.item.map((itemData,index) => {
                                            return <CartItem key={index} cartItem={itemData} />
                                        }) :
                                        [...Array(3)].map((elementInArray, index) => ( 
                        
                                            <CartItem key={index}  isLoading={true} />
                                        ))
                                    }
                                        
                                </tbody>
                            </table>
                        </div>
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="continue__btn">
                                    <a href="/shop">Continue Shopping</a>
                                </div>
                            </div>
                            {/* <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="continue__btn update__btn">
                                    <a href="/#null"><i className="fa fa-spinner"></i> Update cart</a>
                                </div>
                            </div> */}
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="cart__total">
                            <h6>Cart total</h6>
                            <ul>
                                <li>Subtotal <span>₹{cartData.cart.total}</span></li>
                                <li>Total <span>₹{cartData.cart.total}</span></li>
                            </ul>
                            <a href="/#null" className="primary-btn">Proceed to checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
  )
}

export default Cart