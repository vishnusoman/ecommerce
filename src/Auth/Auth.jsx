import React, { useState, useEffect } from "react";
import Login from "../Pages/Login";

function Auth(props) {
    
    const [userKey,setUserKey] = useState(localStorage.getItem('jwt'))

    useEffect(() => { 
      setUserKey(localStorage.getItem('jwt')) 
    },[userKey])
        
  return (
    <>
    {(userKey)?props.children:<Login keySet={setUserKey} />}
      
    </>
  )
}

export {
    Auth
}