import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Routes, Route } from "react-router-dom";
import axios from "axios";
import Header from './Components/Header';
import Home from './Pages/Home';
import Footer from './Components/Footer';
import Shop from './Pages/Shop';
import Login from './Pages/Login';
import { LoginContextProvider } from './Context/LoginContext';
import { Auth } from './Auth/Auth';
import { Provider } from 'react-redux'
import store from './Store/Store'
import Cart from './Pages/Cart';
import Product from './Pages/Product';


function App() {
    const username = process.env.REACT_APP_API_USERNAME;
    const password = process.env.REACT_APP_API_PASSWORD;
    const credentials = btoa(username + ':' + password);
    const basicAuth = 'Basic ' + credentials;

    axios.interceptors.request.use(function (config) {
      config.baseURL = 'https://localhost/wordpress/' 
      if(!config.url.includes('rest_route')) config.headers['Authorization'] = basicAuth 
      config.headers['Access-Control-Allow-Origin'] = '*' 
      config.headers['alg'] = 'HS256' 
      config.headers['typ'] = 'JWT' 
      return config;
    }, function (error) {
      return Promise.reject(error);
    });

    
    
  return (
    <Provider store={store}>
      <LoginContextProvider >
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/" exact={true} element={<Home/>} />
            <Route path="/shop" element={<Auth><Shop/></Auth>} />
            <Route path="/login" element={<Login />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/product/:id" element={<Product />} />
          </Routes>
          <Footer/>
        </BrowserRouter>
      </LoginContextProvider>
    </Provider>

  );
}

export default App;
