import { useEffect, useState } from 'react'
import { Placeholder } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { removeCartItem, updateCartItem } from '../../Store/Slice/CartSlice'


function CartItem({cartItem,isLoading}) {
    const dispatch = useDispatch()
    const cartData = useSelector((prev) => {return prev})
    const [count,setCount] = useState({})

    const changeQuantity = (id,quantity) => {
        setCount(values => ({...values, [id]: quantity}))
        dispatch(updateCartItem({id,quantity}))
    }

    const changeQuantityByArrow = (id,type) => {
        let quantity
        switch (type) {
            case 'decrease': 
                quantity = count[id] - 1
                dispatch(updateCartItem({id,quantity}))
            break;
            case 'increase': 
                quantity = count[id] + 1
                dispatch(updateCartItem({id,quantity}))
            break;
            default:
            break;
        }
        
    }




    useEffect(() => {
        cartData.cart.item.map((obj) => {
            return setCount(values => ({...values, [obj.item_key]: obj.quantity.value}))
        })
    },[cartData])

    return (
        <>
        <tr key={!isLoading && cartItem.item_key}>
            <td className="product__cart__item">
                <div className="product__cart__item__pic">
                    {
                        !isLoading ? <img src={cartItem.featured_image} alt="" width="90px" /> :
                        <Placeholder animation="glow">
                            <Placeholder   style={{ height: '90px', width: '90px' }}/>
                        </Placeholder>
                    }
                </div>
                <div className="product__cart__item__text">
                    
                    {
                        !isLoading ? <h6>{cartItem.name}</h6>:
                        <h6>
                            <Placeholder animation="glow">
                                <Placeholder className="w-100"  style={{ height: '10px' }}/>
                            </Placeholder>
                        </h6>
                    }
                    {
                        !isLoading ? <h5>${cartItem.price}</h5> : 
                        <h5>
                            <Placeholder animation="glow">
                                <Placeholder className="col-3"  style={{ height: '10px' }}/>
                            </Placeholder>    
                        </h5>
                    }
                </div>
            </td>
            {
                !isLoading && 
                <>
                    <td className="quantity__item">
                        <div className="quantity">
                            <div className="pro-qty-2">
                                <i className="fa fa-angle-left" aria-hidden="true" onClick={()=>changeQuantityByArrow(cartItem.item_key,'decrease')}></i>
                                <input onChange={(e)=>changeQuantity(cartItem.item_key,e.target.value)} type="text" name={!isLoading && cartItem.item_key} defaultValue={!isLoading && count[cartItem.item_key]} />
                                <i className="fa fa-angle-right" aria-hidden="true" onClick={()=>changeQuantityByArrow(cartItem.item_key,'increase')}></i>
                            </div>
                        </div>
                    </td>
                    <td className="cart__price">$ {!isLoading && cartItem.price * cartItem.quantity.value}</td>
                    <td className="cart__close"><i className="fa fa-close" onClick={() => { dispatch(removeCartItem(cartItem.item_key)) }}></i></td>
                </>

            }
        </tr>
        </>
    )
}

export default CartItem