import {useEffect, useState} from 'react'
import axios from 'axios'
import Skeleton from 'react-loading-skeleton';

function Categories({filterSet,filterData}) {
    const [categories, setCategories] = useState([])

    useEffect(() => {
    axios.get('/wp-json/wc/v3/products/categories'
    ).then((response) => {
        setCategories(response.data)
    });
}, []);

const categorySelect = (id) => {
    let categoryFilter = {...filterData};
    categoryFilter.category = id
    categoryFilter.page = 1
    filterSet(categoryFilter)
}
  return (
    <ul >
        {
            (categories.length)?
            categories.map((data) => (
                <li key={data.id}><a href="/#null" onClick={(e) => { e.preventDefault(); categorySelect(data.id)}}>{data.name} ({data.count})</a></li>
            )) : <Skeleton height={15} width ={'100%'} count={10} />
        }
        
    </ul>
  )
}

export default Categories