import { Placeholder } from 'react-bootstrap'

function Breadcrumb({path,isLoading}) {
    let [current] = path.slice(-1)
  return (
    <section className="breadcrumb-option">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="breadcrumb__text">
                        {
                            isLoading === true ? 
                            <Placeholder as="h4" animation="wave">
                                <Placeholder xs={2}  size="xs"/>
                            </Placeholder> : 
                            <h4>{current.name}</h4>
                        }
                        <div className="breadcrumb__links">
                            {path.map((data) => ( 
                                (data.path)?
                                <a key={data.path?data.path:0} href={data.path}>{data.name}</a> :
                                
                            (isLoading === true) ? 
                            <Placeholder as="span" animation="wave">
                                <Placeholder xs={5}  size="xs"/>
                            </Placeholder> : 
                                <span key={data.path?data.path:0}>{data.name}</span>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Breadcrumb