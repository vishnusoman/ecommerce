import Categories from "./Categories"
import Tags from "./Tags"
import {useState} from 'react'

function ProductSidebar({categoryfilter,filterData}) {
    const [search, setSearch] = useState('')
    const searchHandle = (e) => { 
        e.preventDefault();
        let searchFilter = {...filterData};
        searchFilter.search = search
        searchFilter.page = 1
        searchFilter.category = ''
        searchFilter.tag = ''
        categoryfilter(searchFilter)
    }
  return (
    <div className="col-lg-3">
        <div className="shop__sidebar">
            <div className="shop__sidebar__search">
                <form onSubmit={(e) => { searchHandle(e) }}>
                    <input type="text" value={search} placeholder="Search..." onChange={(e) => { setSearch(e.target.value) }} />
                    <button type="submit"><span className="icon_search"></span></button>
                </form>
            </div>
            <div className="shop__sidebar__accordion">
                <div className="accordion" id="accordionExample">
                    <div className="card">
                        <div className="card-heading">
                            <a href="/#null" data-toggle="collapse" data-target="#collapseOne">Categories</a>
                        </div>
                        <div id="collapseOne" className="collapse show" data-parent="#accordionExample">
                            <div className="card-body">
                                <div className="shop__sidebar__categories">
                                    <Categories filterSet={categoryfilter} filterData={filterData} />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    {/* <div className="card">
                        <div className="card-heading">
                            <a data-toggle="collapse" data-target="#collapseThree">Filter Price</a>
                        </div>
                        <div id="collapseThree" className="collapse show" data-parent="#accordionExample">
                            <div className="card-body">
                                <div className="shop__sidebar__price">
                                    <ul>
                                        <li><a href="/#">$0.00 - $50.00</a></li>
                                        <li><a href="/#">$50.00 - $100.00</a></li>
                                        <li><a href="/#">$100.00 - $150.00</a></li>
                                        <li><a href="/#">$150.00 - $200.00</a></li>
                                        <li><a href="/#">$200.00 - $250.00</a></li>
                                        <li><a href="/#">250.00+</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> */}
                    
                    <div className="card">
                        <div className="card-heading">
                            <button className="btn" data-toggle="collapse" data-target="#collapseSix">Tags</button>
                        </div>
                        <div id="collapseSix" className="collapse show" data-parent="#accordionExample">
                            <div className="card-body">
                                <Tags filterSet={categoryfilter} filterData={filterData} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ProductSidebar