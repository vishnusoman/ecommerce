//import {Placeholder} from 'react-bootstrap'
import { useEffect } from 'react'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import {useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { addToCart } from '../../Store/Slice/CartSlice'

function Grid({id,title,img,price,loading,average_rating}) {
    const navigate = useNavigate()
    const imgStyle = {
        'backgroundImage': 'url('+img+')'
    }
    const dispatch = useDispatch()

    useEffect(() => {
        //console.log(cartData)
    }, []);
   // console.log(cartData)

    
  return (
      
    <div className="product__item" item-id={id} onClick={() => { navigate('/product/'+id) }}>
        {loading ? <Skeleton height={260} /> : 
            <div className="product__item__pic set-bg" style={imgStyle}>
                
                <ul className="product__hover">
                    <li><a href="/#null"><img src="assets/img/icon/heart.png" alt="" /></a></li>
                    <li><a href="/#null"><img src="assets/img/icon/search.png" alt="" /></a></li>
                </ul>
            </div>
        }
         
        <div className="product__item__text">
            {loading ?<Skeleton height={15} width ={'70%'} />:<h6>{title}</h6>}
            { !loading && <a  href="/#null" onClick={(e) => { e.preventDefault(); dispatch(addToCart(id))}} className="add-cart">+ Add To Cart</a> }
            {loading ?<Skeleton height={15} width ={'50%'} />:
                <div className="rating">
                    <i className={`fa  mr-1 ${average_rating>=1 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${average_rating>=2 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${average_rating>=3 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${average_rating>=4 ? 'fa-star':'fa-star-o'}`}></i>
                    <i className={`fa mr-1 ${average_rating>=5 ? 'fa-star':'fa-star-o'}`}></i>
                </div>
            }
            {loading ?<Skeleton height={15} width ={'30%'} />:
            <h5>{price}</h5>
            }
        </div>
    </div>
  )
}

export default Grid