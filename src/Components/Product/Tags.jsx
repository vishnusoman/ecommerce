import {useEffect, useState} from 'react'
import axios from 'axios'
import Placeholder from 'react-bootstrap/Placeholder'

function Tags({filterSet,filterData}) {
    const [tags, setTags] = useState([])

    useEffect(() => {
    axios.get('/wp-json/wc/v3/products/tags'
    ).then((response) => {
        setTags(response.data)
        
    });
}, []);

const filterSelect = (id) => {
    let tagFilter = {...filterData};
    tagFilter.tag = id
    filterSet(tagFilter)
}
  return (
    <div className="shop__sidebar__tags">
        {
            (tags.length)? tags.map((data) => (
                <a key={data.id} href="#none" onClick={(e) => { e.preventDefault(); filterSelect(data.id)}}>{data.name}</a>
            )) : <Placeholder  animation="glow">
                    <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} />{' '}
                    <Placeholder xs={6} /> <Placeholder xs={8} />
                </Placeholder>
        }
        
    </div>
  )
}

export default Tags