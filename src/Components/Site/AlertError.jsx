import React from 'react'
import Alert from 'react-bootstrap/Alert';

function AlertError({errorData}) {
    //console.log(errorData)
  return (
    <>
    <Alert variant={errorData.type}>{errorData.message}</Alert>
    </>
  )
}

export default AlertError