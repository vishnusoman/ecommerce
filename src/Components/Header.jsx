
import {Link, NavLink } from "react-router-dom";
import { useLoginContext } from "../Context/LoginContext";
import {useDispatch, useSelector} from 'react-redux'
import { Toaster } from "react-hot-toast";
import { useEffect } from "react";
import { getCartData } from "../Store/Slice/CartSlice";

function Header() {
    const loginContext = useLoginContext();
    const cartData = useSelector((prev) => { return prev })
    const dispatch = useDispatch()

    const changeUserData = (e) => { 
        e.preventDefault()
        loginContext.setUserData({
            id: 99,
            name: 'kannan'
        }) 
    }

    useEffect(() => {
        dispatch(getCartData())
    }, [dispatch]);
  return (
    <>
    <Toaster />
    <header className="header">
        <div className="header__top">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-7">
                        <div className="header__top__left">
                            <p>Free shipping, 30-day return or refund guarantee.</p>
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-5">
                        <div className="header__top__right">
                            <div className="header__top__links">
                                <a href="/#" onClick={changeUserData}>Sign in</a>
                                <a href="/#">FAQs</a>
                            </div>
                            <div className="header__top__hover">
                                <span>Usd <i className="arrow_carrot-down"></i></span>
                                <ul>
                                    <li>USD</li>
                                    <li>EUR</li>
                                    <li>USD</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg-3 col-md-3">
                    <div className="header__logo">
                        <Link to="/" ><img src="http://localhost:3000/assets/img/logo.png" alt="" /></Link>
                    </div>
                </div>
                <div className="col-lg-6 col-md-6">
                    <nav className="header__menu mobile-menu">
                        <ul>
                            <li><NavLink exact="true" to='/'  >Home</NavLink></li>
                            <li><NavLink to="/shop" >Shop</NavLink></li>
                            <li><a href="/#">Pages</a>
                                <ul className="dropdown">
                                    <li><a href="./about.html">About Us</a></li>
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shopping-cart.html">Shopping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li>
                            <li><a href="./blog.html">Blog</a></li>
                            <li><a href="./contact.html">Contacts</a></li>
                        </ul>
                    </nav>
                </div>
                <div className="col-lg-3 col-md-3">
                    <div className="header__nav__option">
                        <a href="/#" className="search-switch"><img src="http://localhost:3000/assets/img/icon/search.png" alt="" /></a>
                        <a href="/#"><img src="http://localhost:3000/assets/img/icon/heart.png" alt="" /></a>
                        <a href="/cart"><img src="http://localhost:3000/assets/img/icon/cart.png" alt="" /> <span>{cartData.cart.count}</span></a>
                        <div className="price">₹{cartData.cart.total}</div>
                    </div>
                </div>
            </div>
            <div className="canvas__open"><i className="fa fa-bars"></i></div>
        </div>
    </header>
    </>
  )
}

export default Header