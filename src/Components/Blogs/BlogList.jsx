import React, { Component } from 'react'
import BlogGrid from './BlogGrid'
import axios from 'axios'

export default class BlogList extends Component {
  constructor(){
    super()
    this.state = {
        blogLoad:true ,
        blogList: []
    }
  }
  componentDidMount(){
        axios.get('/wp-json/wp/v2/posts?_embed'
        ).then((response) => {
            this.setState({
                blogList: response.data,
                blogLoad:false
            })
        });
  }
  
  render() {
    return (
      <section className="latest spad pt-0">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="section-title">
                        <span>Latest News</span>
                        <h2>Fashion New Trends</h2>
                    </div>
                </div>
            </div>
            <div className="row">
                {   (this.state.blogList.length > 0) ?
                    this.state.blogList.map((data) => { return(
                        <BlogGrid data={data} key={data.id} loading={this.state.blogLoad}/>
                    ) }) : 
                    [...Array(3)].map((elementInArray, index) => ( 
                        <BlogGrid key={index} loading={this.state.blogLoad}/>
                    ))
                }
            </div>
        </div>
    </section>
    )
  }
}
