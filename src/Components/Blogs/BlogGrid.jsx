import React, { Component } from 'react'
import { Placeholder } from 'react-bootstrap'
import Moment from 'moment';

export default class BlogGrid extends Component {


  render() {
    //console.log(this.props.data)
    return (
      <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="blog__item">
              {(this.props.loading) ? <Placeholder animation="glow"><Placeholder className="w-100"  style={{ height: '270px' }}/></Placeholder> :
                <div className="blog__item__pic set-bg" style={{'background': `url(${this.props.data._embedded['wp:featuredmedia']['0'].source_url})`}}></div>}
                
                {(!this.props.loading) ? 
                  <div className="blog__item__text">
                      <span><img src="assets/img/icon/calendar.png" alt="" /> {Moment(this.props.data.date).format('DD MMMM YYYY')}</span>
                      <h5>{this.props.data.title.rendered}</h5>
                      <a href="/#">Read More</a>
                  </div>
                  :
                  <div className=""> <Placeholder animation="glow"><Placeholder xs={7} /> <Placeholder xs={4} /> 
                      <Placeholder xs={6} /> <Placeholder xs={8} /></Placeholder> 
                  </div>
                }
            </div>
        </div>
    )
  }
}
