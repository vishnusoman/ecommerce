import React, {createContext, useState, useContext} from 'react'

const LoginContext = createContext({})



export function LoginContextProvider(props){
    const [userData,setUserData] = useState([])


    return (
    <LoginContext.Provider value={{userData,setUserData}}>
      {props.children}
    </LoginContext.Provider>
  );
  
}

export const useLoginContext = () => useContext(LoginContext);



